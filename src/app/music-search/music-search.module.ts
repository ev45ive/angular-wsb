import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicSearchRoutingModule } from './music-search-routing.module';
import { AlbumSearchViewComponent } from './container/album-search-view/album-search-view.component';
import { AlbumDetailsViewComponent } from './container/album-details-view/album-details-view.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { AlbumGridComponent } from './components/album-grid/album-grid.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    AlbumSearchViewComponent,
    AlbumDetailsViewComponent,
    SearchFormComponent,
    AlbumGridComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MusicSearchRoutingModule
  ]
})
export class MusicSearchModule { }
