import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from 'src/app/core/model/Playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.scss']
})
export class PlaylistDetailsComponent implements OnInit {

  @Input() playlist!: Playlist

  @Output() edit = new EventEmitter();

  constructor() { }

  editClicked() {
    this.edit.emit()
  }

  ngOnInit(): void {
    if (!this.playlist) {
      throw new Error('Playlist is required!')
    }
  }

  getMyStyles() {
    return {
      'font-size.px': 2 * 10, 'border-bottom-width.px': 2
    }
  }

  getMyClasses() {
    return {
      publicPlaylist: this.playlist.public,
      privatePlaylist: !this.playlist.public
    }
  }

}
